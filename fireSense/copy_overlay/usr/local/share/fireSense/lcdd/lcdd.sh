#!/bin/sh
#
# PROVIDE: lcdd
# REQUIRE: 
# BEFORE:  hostid
# KEYWORD:
#

. /etc/rc.subr

name="lcdd"
start_cmd="lcdd_start"
stop_cmd="lcdd_stop"

PATH=/usr/local/bin:/usr/local/sbin:/bin:/sbin:/usr/bin:/usr/sbin
PATH_INSTALL_BASE="/usr/local/share/fireSense"

# lcdproc flags
#
#   [C]pu
#   [G]raph
#   [T]ime
#   [M]emory
#   [X]load
#   [D]isk
#   [B]attery
#   proc_[S]izes
#   [O]ld_time
#   big_cloc[K]
#   [U]ptime
#   CPU_SM[P]
#   [A]bout
#        
            
lcdd_start() {
    nice -20 ${PATH_INSTALL_BASE}/lcdd/LCDd -r 0 -c ${PATH_INSTALL_BASE}/lcdd/LCDd.conf > /dev/null 2>&1 &
    nice -20 ${PATH_INSTALL_BASE}/lcdd/lcdproc C M P &
}

lcdd_stop() {}

load_rc_config $name
run_rc_command "$1"
