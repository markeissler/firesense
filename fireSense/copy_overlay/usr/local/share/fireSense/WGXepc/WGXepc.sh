#!/bin/sh
#
# PROVIDE: wgxepc
# REQUIRE: firesense pf
# BEFORE:  
# KEYWORD:
#

. /etc/rc.subr

name="wgxepc"
start_cmd="wgxepc_start"
stop_cmd="wgxepc_stop"

PATH=/usr/local/bin:/usr/local/sbin:/bin:/sbin:/usr/bin:/usr/sbin
PATH_INSTALL_BASE="/usr/local/share/fireSense"

wgxepc_start() {
    ${PATH_INSTALL_BASE}/WGXepc/WGXepc -l green > /dev/null 2>&1
}

wgxepc_stop() {}

load_rc_config $name
run_rc_command "$1"
