#!/bin/sh
#
# PROVIDE: serialbandaid
# REQUIRE: firesense
# BEFORE:  LOGIN
# KEYWORD:
#

. /etc/rc.subr

name="serialbandaid"
start_cmd="serialbandaid_start"
stop_cmd="serialbandaid_stop"
status_cmd="serialbandaid_status"
extra_commands="status"

trap serialbandaid_start 1 2 3 6 9 14 15

RETVAL=0

serialbandaid_start() {
    # make sure we're not already running! if so, don't do anything
    serialbandaid_status &>/dev/null
    RETVAL=$?

    if [ "$RETVAL" -eq 0 ]; then        
        # fire up the runner to keep us alive
        _runner
        
        echo "SerialBandAid has been started"
        echo
    else
        echo "SerialBandAid is already running"
        echo
    fi
}

serialbandaid_stop() {
    # don't include me
    MY_PID=$$
    
    ps -x | grep 'serialbandaid' | grep -v '^${MY_PID}' | grep -v 'start' | grep -v 'stop' | grep -v grep | awk '{print $1}' | xargs kill -9 > /dev/null 2>&1
    RETVAL=$?
    sleep 3
}

serialbandaid_restart() {
    serialbandaid_stop;
    # avoid race
    sleep 3
    serialbandaid_start;
}

serialbandaid_status() {
    # don't include me
    MY_PID=$$
    
    PID=$(ps -ax | grep 'serialbandaid' | grep -v ${MY_PID} | grep -v 'status' | grep -v ' grep ' | awk '{print $1}') > /dev/null 2>&1
    
    if [ -n "${PID}" ]; then
        echo "SerialBandAid is running"
        echo
        return 1
    else
        echo "SerialBandAid is stopped"
        echo
        return 0
    fi
    
    exit 0
}

_runner() {
    while :
    do
        # kill off previous instances
        serialbandaid_stop;
        
        # start a new process
        /etc/rc.initial > /dev/ttyu0 < /dev/ttyu0
        sleep 3
    done
    
    exit 0
}

load_rc_config $name
run_rc_command "$1"
