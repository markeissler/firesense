#!/bin/sh
#
# fireSense_setup.sh
#
# fireSense setup script. Run after install or change to setup
# file permissions properly.
#
# PROVIDE: firesense
# REQUIRE: FILESYSTEMS
# BEFORE:  cleanvar
# KEYWORD:
#

PATH=/usr/local/bin:/usr/local/sbin:/bin:/sbin:/usr/bin:/usr/sbin
PATH_INSTALL_BASE="/usr/local/share/fireSense"
#
# NOTE: We no longer add to device.hints because that file is 
# updated by the FreeBSD maintainers. Instead, add all changes
# to loader.conf!
#
PATH_LOADER_CONF="/boot/loader.conf"
PATH_SYSCTL_CONF="/etc/sysctl.conf"
PATH_TTYS_CONF="/etc/ttys"
PATH_GETTYTAB="/etc/gettytab"

# LICENSE PATHS
#
PATH_LICENSE_INTEL_IPW_MONITOR="/usr/share/doc/legal/intel_ipw/"
PATH_LICENSE_INTEL_IPW_BSS="/usr/share/doc/legal/intel_ipw/"
PATH_LICENSE_INTEL_WPI="/usr/share/doc/legal/intel_wpi/"

# FIXUPS
#
FIX_GETTYTAB=1
FIX_UARTS=1

RUNONCE="/usr/local/etc/fireSense.setup"

trap 'echo "finalizing fireSense overlay" ; rm -f ${RUNONCE}' 1 2 3 15

if test -e ${RUNONCE} ; then
    echo ""
    echo " This script has been executed previously."
    echo ""
    echo " Are you sure you want to re-add this package? It may overwrite custom settings."
    echo " rm ${RUNONCE} to allow the re-execution of this script."
    echo ""
    exit 1
else
    echo " Making post-installation modifications now."
    touch ${RUNONCE}
fi


# set filesystem to rw
#
/etc/rc.conf_mount_rw

#
# setup base
echo "Setting up fireSense base..."
chown -R root:wheel ${PATH_INSTALL_BASE}
chmod -R ugo=rX ${PATH_INSTALL_BASE}

#
# setup lcdd
#
echo "Installing LCD support..."
chown -R root:wheel ${PATH_INSTALL_BASE}/lcdd
chmod -R ugo=rX ${PATH_INSTALL_BASE}/lcdd

# no longer doing this since we now startup from /etc/rc.bootup
#
#cp ${PATH_INSTALL_BASE}/lcdd/lcdd.sh /usr/local/etc/rc.d/
#chown root:wheel /usr/local/etc/rc.d/lcdd.sh
#chmod ugo=rx /usr/local/etc/rc.d/lcdd.sh

# no longer need these links since stephen's lcdd 5.
#
#ln -s /usr/lib/libc.so /usr/local/lib/libc.so.6
#ln -s /usr/lib/libkvm.so /usr/local/lib/libkvm.so.3
#ln -s /usr/lib/libkvm.so /usr/local/lib/libkvm.so.4

# update config file
CONFLINE="DriverPath=${PATH_INSTALL_BASE}/lcdd/drivers/"
if grep "^DriverPath" ${PATH_INSTALL_BASE}/lcdd/LCDd.conf >/dev/null; then
    sed -i "" -e "s|^DriverPath.*$|$CONFLINE|g" ${PATH_INSTALL_BASE}/lcdd/LCDd.conf
else
    echo ${CONFLINE} >> ${PATH_INSTALL_BASE}/lcdd/LCDd.conf
fi


#
# setup arm/disarm
#
echo "Installing Arm/Disarm LED support..."
chown -R root:wheel ${PATH_INSTALL_BASE}/WGXepc
chmod -R ugo=rX ${PATH_INSTALL_BASE}/WGXepc

cp ${PATH_INSTALL_BASE}/WGXepc/WGXepc.sh /usr/local/etc/rc.d/
chown root:wheel /usr/local/etc/rc.d/WGXepc.sh
chmod ugo=rx /usr/local/etc/rc.d/WGXepc.sh


#
# setup serial fix
#
echo "Setting up Serial Console fix..."
chown -R root:wheel ${PATH_INSTALL_BASE}/SerialBandAid
chmod -R ugo=rX ${PATH_INSTALL_BASE}/SerialBandAid

cp ${PATH_INSTALL_BASE}/SerialBandAid/serialbandaid.sh /usr/local/etc/rc.d/
chown root:wheel /usr/local/etc/rc.d/serialbandaid.sh
chmod ugo=rx /usr/local/etc/rc.d/serialbandaid.sh

#
# boot fixes
#

# enable/disable ACPI
#
CONFLINE='hint.acpi.0.disabled="1"'
if grep "^hint.acpi.0.disabled" ${PATH_LOADER_CONF} >/dev/null; then
    sed -i "" -e "s/^hint.acpi.0.disabled.*$/$CONFLINE/g" ${PATH_LOADER_CONF}
else
    # sometimes existing params are missing a newline, so we make sure
    # there is one by rewriting the file with newlines first, then we
    # append our new param.
    awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp
    echo ${CONFLINE} >> ${PATH_LOADER_CONF}.tmp
    cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
    rm ${PATH_LOADER_CONF}.tmp
fi

# fix ACPI reboot
#
if [ "${CONFLINE}" = 'hint.acpi.0.disabled="0"' ]; then
    CONFLINE='hw.acpi.disable_on_reboot="1"'
    if grep "^hw.acpi.disable_on_reboot" ${PATH_LOADER_CONF} >/dev/null; then
        sed -i "" -e "s/^hw.acpi.disable_on_reboot.*$/$CONFLINE/g" ${PATH_LOADER_CONF}
    else
        # sometimes existing params are missing a newline, so we make sure
        # there is one by rewriting the file with newlines first, then we
        # append our new param.
        awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp
        echo ${CONFLINE} >> ${PATH_LOADER_CONF}.tmp
        cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
        rm ${PATH_LOADER_CONF}.tmp     
    fi
fi

# enable/disable APIC
#
# Disable apic if the system suffers from interrupt storms and/or calcru
# errors.
#
CONFLINE='hint.apic.0.disabled="1"'
if grep "^hint.apic.0.disabled" ${PATH_LOADER_CONF} >/dev/null; then
     sed -i "" -e "s/^hint.apic.0.disabled.*$/$CONFLINE/g" ${PATH_LOADER_CONF}
else
     # sometimes existing params are missing a newline, so we make sure
     # there is one by rewriting the file with newlines first, then we
     # append our new param.
     awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp
     echo ${CONFLINE} >> ${PATH_LOADER_CONF}.tmp
     cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
     rm ${PATH_LOADER_CONF}.tmp
fi


# disable DMA
#
CONFLINE='hw.ata.ata_dma="0"'
if grep "^hw.ata.ata_dma" ${PATH_LOADER_CONF} >/dev/null; then
     sed -i "" -e "s/^hw.ata.ata_dma.*$/$CONFLINE/g" ${PATH_LOADER_CONF}
else
     # sometimes existing params are missing a newline, so we make sure
     # there is one by rewriting the file with newlines first, then we
     # append our new param.
     awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp
     echo ${CONFLINE} >> ${PATH_LOADER_CONF}.tmp
     cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
     rm ${PATH_LOADER_CONF}.tmp     
fi


#
# ipw_bss, ipw_monitor LICENSE prompt
#

# TODO! (NEED TO PROMPT USER FOR ACCEPTANCE)

## ipw_bss / ipw_monitor
CONFLINE='legal.intel_ipw.license_ack="1"'
if grep "^legal.intel_ipw.license_ack" ${PATH_LOADER_CONF} >/dev/null; then
     sed -i "" -e "s/^legal.intel_ipw.license_ack.*$/$CONFLINE/g" ${PATH_LOADER_CONF}
else
     # sometimes existing params are missing a newline, so we make sure
     # there is one by rewriting the file with newlines first, then we
     # append our new param.
     awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp
     echo ${CONFLINE} >> ${PATH_LOADER_CONF}.tmp
     cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
     rm ${PATH_LOADER_CONF}.tmp
fi

## wpi
CONFLINE='legal.intel_wpi.license_ack="1"'
if grep "^legal.intel_wpi.license_ack" ${PATH_LOADER_CONF} >/dev/null; then
     sed -i "" -e "s/^legal.intel_wpi.license_ack.*$/$CONFLINE/g" ${PATH_LOADER_CONF}
else
     # sometimes existing params are missing a newline, so we make sure
     # there is one by rewriting the file with newlines first, then we
     # append our new param.
     awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp
     echo ${CONFLINE} >> ${PATH_LOADER_CONF}.tmp
     cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
     rm ${PATH_LOADER_CONF}.tmp
fi

# sysctl
#
# hardwire kern timer to TSC to avoid calcru errors
#
CONFLINE='kern.timecounter.hardware="TSC"'
if grep "^kern.timecounter.hardware" ${PATH_SYSCTL_CONF} >/dev/null; then
     sed -i "" -e "s/^kern.timecounter.hardware.*$/$CONFLINE/g" ${PATH_SYSCTL_CONF}
else
     # sometimes existing params are missing a newline, so we make sure
     # there is one by rewriting the file with newlines first, then we
     # append our new param.
     awk 1 ${PATH_SYSCTL_CONF} > ${PATH_SYSCTL_CONF}.tmp
     echo ${CONFLINE} >> ${PATH_SYSCTL_CONF}.tmp
     cp ${PATH_SYSCTL_CONF}.tmp ${PATH_SYSCTL_CONF}
     rm ${PATH_SYSCTL_CONF}.tmp
fi

#
# fix ttys
#

# turn off existing incorrect entries
#
# console: single user mode, if this is on then no other terminals will
# be available.
#
# ttyv[0-8]: video terminals
# ttyu[0-8]: serial terminals
#
if grep "^console" ${PATH_TTYS_CONF} >/dev/null; then
    sed -i "" -e "/^console /{s/ on/ off/;}" ${PATH_TTYS_CONF}
fi
if grep "^ttyv" ${PATH_TTYS_CONF} >/dev/null; then
    sed -i "" -e "/^ttyv[0-8] /{s/ on/ off/;}" ${PATH_TTYS_CONF}
fi
if grep "# firesense" ${PATH_TTYS_CONF} >/dev/null; then
    # remove existing fireSense entries
    sed -i "" -e "/# fireSense/,/^ttyu3.*secure/d" ${PATH_TTYS_CONF}
elif grep "^ttyu" ${PATH_TTYS_CONF} >/dev/null; then
    # remove existing ttyu0 entries
    sed -i "" -e "/^ttyu[0-8] /{s/ on/ off/;}" ${PATH_TTYS_CONF}
fi


# add proper entries for serial access
#
# DO NOT add extra # infront of fireSense label or you'll break
# future parsing!
#

cat >> ${PATH_TTYS_CONF} << EOF
# fireSense
#
# ttyu0 - ttyu3 correspond to COM1 thru COM4 (we only have ttyu0)
#
# reboot or restart init (>kill -HUP 1) if you make changes below!
#
ttyu0   "/usr/libexec/getty bootupcli"  cons25  off secure
ttyu1   "/usr/libexec/getty std.9600"   dialup  off secure
ttyu2   "/usr/libexec/getty std.9600"   dialup  off secure
ttyu3   "/usr/libexec/getty std.9600"   dialup  off secure
EOF

# fix gettytab
#
# replace serial user with admin, whose shell is set to rc.initial
#
# Search for this:
#
#   bootupcli:\
#        :tc=std.9600:\
#        :ht:np:sp#115200:al=root:
#
# Replace with this:
#
#   bootupcli:\
#        tc=std.9600:\
#        :ht:np:sp#9600:al=admin:
#
# We use a sed range to search for a block that starts wtih "bootupcli:"
# and ends with "root:". Then we delete everything except the part that
# starts with "bootupcli", finally replacing that "bootupcli" with the
# replacement block.
#
#  sed -e "/bootupcli:/,/root:/{/^bootupcli/!d;s/^bootupcli.*/${CONFLINE}/;}" ${PATH_GETTYTAB}
#
# This is doing it the long way around, and only works if we have a 3
# line entry (the formatting on multiple lines is important!
#
#   /^bootupcli:/ {
#        N
#        /\n.*/ {
#           N
#           /\n.*root:/ {
#                s/^bootupcli:.*\n.*\n.*root:/${CONFLINE}/
#           }
#        }
#   }" ${PATH_GETTYTAB}
#
# NOTE: Yes, we are changing auto-login user to admin here.
#
CONFLINE='bootupcli:\
        :tc=std.9600:\
        :ht:np:sp#9600:al=admin:'

if [ "${FIX_GETTYTAB}" -ne 0 ]; then 
    # sometimes existing params are missing a newline, so we make sure
    # there is one by rewriting the file with newlines first, then we
    # append our new param.
    awk 1 ${PATH_GETTYTAB} > ${PATH_GETTYTAB}.tmp

    if grep "bootupcli:" ${PATH_GETTYTAB}.tmp >/dev/null; then
         sed -i "" -e "/^bootupcli:/,/root:$/{/^bootupcli/!d;s/^bootupcli.*/${CONFLINE}/;}" ${PATH_GETTYTAB}.tmp
    else
         echo ${CONFLINE} >> ${PATH_GETTYTAB}.tmp
    fi
    cp ${PATH_GETTYTAB}.tmp ${PATH_GETTYTAB}
    rm ${PATH_GETTYTAB}.tmp
fi

# fix uart (TODO)
#
# sio was replaced with uart for pfsense 2.x
#
#  hint.uart.0.at="isa"
#  hint.uart.0.port="0x3F8"
#  hint.uart.0.flags="0x10"
#  hint.uart.0.irq="4"
#  hint.uart.1.at="isa"
#  hint.uart.1.port="0x2F8"
#  hint.uart.1.irq="3"
#  hint.uart.2.disabled="1"
#  hint.uart.3.disabled="1"
#

if [ "${FIX_UARTS}" -ne 0 ]; then 
    # sometimes existing params are missing a newline, so we make sure
    # there is one by rewriting the file with newlines first, then we
    # append our new param.
    awk 1 ${PATH_LOADER_CONF} > ${PATH_LOADER_CONF}.tmp

    # clear out all existing uart entries 
    if grep "^hint.uart.[0-3]" ${PATH_LOADER_CONF}.tmp >/dev/null; then
         # clear out all existing uart entries
         sed -i "" -e "/^hint.uart.[0-3].*/{/.*/d;}" ${PATH_LOADER_CONF}.tmp
    fi

    # append our updated config
cat >> ${PATH_LOADER_CONF}.tmp << EOF
hint.uart.0.at="isa"
hint.uart.0.port="0x3F8"
hint.uart.0.flags="0x10"
hint.uart.0.irq="4"
hint.uart.0.baud="9600"
hint.uart.1.at="isa"
hint.uart.1.port="0x2F8"
hint.uart.1.irq="3"
hint.uart.1.baud="9600"
hint.uart.2.disabled="1"
hint.uart.3.disabled="1"
EOF
    cp ${PATH_LOADER_CONF}.tmp ${PATH_LOADER_CONF}
    rm ${PATH_LOADER_CONF}.tmp
fi


# force serial/select serial (TODO)
#
# To force serial console put -H in /boot.config, or to only use serial 
# console if no keyboard is pressent when booting, put -P in /boot.config.
#

# set filesystem to ro
#
/etc/rc.conf_mount_ro


# all done!
echo "System modifications are complete. Rebooting..."

shutdown -r now

exit 0
