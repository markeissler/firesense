<?php
/****h* pfSense/config
 * NAME
 *   fireSense.inc - Functions to suppor the fireSense distribution
 * DESCRIPTION
 *   This file includes all of the setup/support functions that help get @author mark
 *   fireSense configuration sane.
 * HISTORY
 * $Id$
 ******

        fireSense.inc
        Copyright (C) 2011 Mark Eissler (mark@mixtur.com)
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright notice,
           this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

        THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
        AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
        AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
        OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
        SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
        INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
        CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
        ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.


        pfSense_MODULE: utils
*/

if($g['booting']) echo ".";

function enable_fireSense_lcd() {
	global $config, $g;

	if($g['booting'])
		echo "Initializing lcd...";
	
	$fireSense_lcdd = "/usr/local/share/fireSense/lcdd/lcdd.sh";

	/* startup lcdd only if fireSense has been setup. */
	if($g['booting'] && is_file("/usr/local/etc/fireSense.setup")) {
		mwexec("$fireSense_lcdd start");
	} else {
		echo "FAIL. fireSense hasn't been setup yet!\n";
		exit(1);
	}

	if($g['booting'])
		echo "done.\n";
}

function kill_fireSense_lcd() {
	$fireSense_lcdd = "/usr/local/share/fireSense/lcdd/lcdd.sh";
	mwexec("$fireSense_lcdd stop");
}

?>
