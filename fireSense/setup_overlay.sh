#!/bin/sh

# Set manually
TOOLSDIR=/home/pfsense/tools/builder_scripts/

# Source pfsense_local.sh variables if possible
if [ -f ./pfsense_local.sh ]; then
        . ./pfsense_local.sh
fi

# Copy pfsense-build.conf into TOOLSDIR
cp pfsense-build.conf  $TOOLSDIR

# Ensure $SRCDIR exists
mkdir -p $SRCDIR

# cd $TOOLSDIR and read in pfsense_local.sh
cd $TOOLSDIR

# Start building
if [ "$1" != "noupdate" ]; then
	echo ">>> noupdate flag NOT passed. Cleaning and updating GIT repo"
        ./clean_build.sh
        ./update_git_repos.sh
        ./apply_kernel_patches.sh
fi

./build_nano.sh

